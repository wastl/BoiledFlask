"""Main App routes"""
from flask import render_template  # , flash, request, redirect, url_for, Markup

# from flask_babelex import _

# from flask_user import login_required, current_user

from app.main import blueprint


@blueprint.route('/')
def index():
    """Index route"""
    return render_template('main/index.html', title='Wastl-B')
