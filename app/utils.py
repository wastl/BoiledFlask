"""Usefull utilities"""
# pylint: disable=wrong-import-position
from datetime import datetime
from slugify import slugify


def log(sender=None, message=None):
    """
    A small custom logger

    example.py:

    .. code-block:: python

        from app.utils import log
        log(__name__, 'Log This!')


    ``example [2018-06-30 10:54:04.655141] Log This!``

    :param sender: string, file which loggs
    :param message: string, which shall be logged
    """
    if sender is None:
        sender = __name__.split('.')[0]
    now = datetime.utcnow()
    print('{0} [{1}] {2}'.format(sender, now, message))


def random_string_generator(size=10, chars=None):
    """
    A simple random string generator

    .. code-block:: python

        import string
        from app.utils import random_string_generator
        randstr = random_string_generator(size=3, chars=string.ascii_uppercase)

    :param size: integer, size of string, defaults to 10
    :param chars:  string of choices, if not provided, **string.ascii_lowercase** & **string.digits** will be taken
    :returns: string, random
    """
    import random
    if chars is None:
        import string
        chars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):
    """
    A unique-slug generator for db models

    Requires a **title** and **slug** field in model. Slug will be bases on title, 
    unless **new_slug** is provided.

    If another instance exists with the slug, a random string will be added with a `-`.

    .. code-block:: python

        import string
        from app import DB 
        from app.models import YourModel
        from app.utils import unique_slug_generator
        obj = YourModel.query.get(1)
        obj.slug = unique_slug_generator(obj)
        DB.commit()

    :param inctance: instance of SQLAlchemy Model
    :param new_slug: string for slug, if not provided, **instance.title** will be taken
    :returns: string (slug)
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title, max_length=255)

    klass = instance.__class__
    obj_exists = klass.query.filter_by(slug=slug).first()
    if obj_exists is not None:
        new_slug = "{slug}-{randstr}".format(slug=slug,
                                             randstr=random_string_generator(size=3))
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug
