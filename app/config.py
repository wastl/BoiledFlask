"""App configuration"""
import os


APP_NAME = "BoiledFlask"

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# ===== SSL ===== #
SSL = False

# ===== WTF Configuration ===== #
# Cross Site Request Forgery protection for WTForms
WTF_CSRF_ENABLED = True
SECRET_KEY = 'SuperSecret'

# ===== Babel Configuration ===== #
LANGUAGES = ['en', 'de']

# ===== DB Config ===== #
# path to your db, uses a simple sqllight file inside project directory if you not set this.
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')

# DEPRECATED: Set value explicitly
SQLALCHEMY_TRACK_MODIFICATIONS = False

# ===== Flask-User settings ===== #
# Shown in and email templates and page footers
USER_APP_NAME = APP_NAME
USER_ENABLE_EMAIL = True
USER_ENABLE_USERNAME = True

USER_ENABLE_REMEMBER_ME = True

USER_ENABLE_REGISTER = False
USER_REQUIRE_RETYPE_PASSWORD = True

USER_ENABLE_INVITE_USER = False
USER_REQUIRE_INVITATION = False

USER_ALLOW_LOGIN_WITHOUT_CONFIRMED_EMAIL = False

USER_EMAIL_SENDER_NAME = USER_APP_NAME
USER_EMAIL_SENDER_EMAIL = "noreply@example.com"

USER_ENABLE_MULTIPLE_EMAILS = False
USER_ENABLE_CHANGE_USERNAME = True
USER_ENABLE_CHANGE_PASSWORD = True
USER_ENABLE_CONFIRM_EMAIL = True
USER_ENABLE_FORGOT_PASSWORD = True

USER_ENABLE_AUTH0 = False

USER_SEND_PASSWORD_CHANGED_EMAIL = True
USER_SEND_REGISTERED_EMAIL = True
USER_SEND_USERNAME_CHANGED_EMAIL = True

USER_SHOW_EMAIL_DOES_NOT_EXIST = False
USER_SHOW_USERNAME_DOES_NOT_EXIST = False

USER_CHANGE_PASSWORD_URL = '/user/change-password'
USER_CHANGE_USERNAME_URL = '/user/change-username'
USER_CONFIRM_EMAIL_URL = '/user/confirm-email/<token>'
USER_EDIT_USER_PROFILE_URL = '/user/edit_user_profile'
USER_EMAIL_ACTION_URL = '/user/email/<id>/<action>'
USER_FORGOT_PASSWORD_URL = '/user/forgot-password'
USER_INVITE_USER_URL = '/user/invite'
USER_LOGIN_URL = '/user/sign-in'
USER_LOGOUT_URL = '/user/sign-out'
USER_MANAGE_EMAILS_URL = '/user/manage-emails'
USER_REGISTER_URL = '/user/register'
USER_RESEND_EMAIL_CONFIRMATION_URL = '/user/resend-email-confirmation'
USER_RESET_PASSWORD_URL = '/user/reset-password/<token>'

USER_CHANGE_PASSWORD_TEMPLATE = 'flask_user/change_password.html'
USER_CHANGE_USERNAME_TEMPLATE = 'flask_user/change_username.html'
USER_EDIT_USER_PROFILE_TEMPLATE = 'flask_user/edit_user_profile.html'
USER_FORGOT_PASSWORD_TEMPLATE = 'flask_user/forgot_password.html'
USER_INVITE_USER_TEMPLATE = 'flask_user/invite_user.html'
USER_LOGIN_TEMPLATE = 'flask_user/login.html'
USER_LOGIN_AUTH0_TEMPLATE = 'flask_user/login_auth0.html'
USER_MANAGE_EMAILS_TEMPLATE = 'flask_user/manage_emails.html'
USER_REGISTER_TEMPLATE = 'flask_user/register.html'
USER_RESEND_CONFIRM_EMAIL_TEMPLATE = 'flask_user/resend_confirm_email.html'
USER_RESET_PASSWORD_TEMPLATE = 'flask_user/reset_password.html'
USER_CONFIRM_EMAIL_TEMPLATE = 'flask_user/emails/confirm_email'
USER_INVITE_USER_EMAIL_TEMPLATE = 'flask_user/emails/invite_user'
USER_PASSWORD_CHANGED_EMAIL_TEMPLATE = 'flask_user/emails/password_changed'
USER_REGISTERED_EMAIL_TEMPLATE = 'flask_user/emails/registered'
USER_RESET_PASSWORD_EMAIL_TEMPLATE = 'flask_user/emails/reset_password'
USER_USERNAME_CHANGED_EMAIL_TEMPLATE = 'flask_user/emails/username_changed'

USER_AFTER_CHANGE_PASSWORD_ENDPOINT = ''
USER_AFTER_CHANGE_USERNAME_ENDPOINT = ''
USER_AFTER_CONFIRM_ENDPOINT = ''
USER_AFTER_EDIT_USER_PROFILE_ENDPOINT = ''
USER_AFTER_FORGOT_PASSWORD_ENDPOINT = ''
USER_AFTER_LOGIN_ENDPOINT = ''
USER_AFTER_LOGOUT_ENDPOINT = ''
USER_AFTER_REGISTER_ENDPOINT = ''
USER_AFTER_RESEND_EMAIL_CONFIRMATION_ENDPOINT = ''
USER_AFTER_RESET_PASSWORD_ENDPOINT = ''
USER_AFTER_INVITE_ENDPOINT = ''
USER_UNAUTHENTICATED_ENDPOINT = 'user.login'
USER_UNAUTHORIZED_ENDPOINT = ''

USER_AUTO_LOGIN = True
USER_AUTO_LOGIN_AFTER_CONFIRM = True
USER_AUTO_LOGIN_AFTER_REGISTER = True
USER_AUTO_LOGIN_AFTER_RESET_PASSWORD = True
USER_AUTO_LOGIN_AT_LOGIN = True

""" The way Flask-User handles case insensitive searches.
Valid options are:
- ‘ifind’ (default): Use the case insensitive ifind_first_object()
- ‘nocase_collation’: username and email fields must be configured
with an case insensitve collation(collation=’NOCASE’ in SQLAlchemy)
so that a regular find_first_object() can be performed. """
USER_IFIND_MODE = 'ifind'

USER_CONFIRM_EMAIL_EXPIRATION = 172800  # 2 days
USER_INVITE_EXPIRATION = 7776000  # 90 days
USER_RESET_PASSWORD_EXPIRATION = 172800  # 2 days

USER_USER_SESSION_EXPIRATION = 3600

# ===== Flask-Mail SMTP server settings for Flask-User ===== #
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USE_TLS = False

MAIL_USERNAME = 'user@example.com'
MAIL_PASSWORD = 'password'

MAIL_DEFAULT_SENDER = '"{}" <user@example.com>'.format(APP_NAME)
