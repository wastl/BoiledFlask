"""User specific routes"""
from flask import render_template, current_app
# from flask_babelex import _
from flask_user.forms import RegisterForm, LoginForm

from app.user import blueprint


@blueprint.route('/enter', methods=['GET'])
def enter():
    """Basic login/register route"""
    login_form = LoginForm()
    register_form = None
    if current_app.config.get('USER_ENABLE_REGISTER'):
        register_form = RegisterForm()
    return render_template('users/register.html',
                           login_form=login_form,
                           register_form=register_form)
