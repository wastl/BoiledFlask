"""
User related db models
"""
from flask_user import UserMixin

from app import DB


class User(DB.Model, UserMixin):
    """User model according to Flask-User"""
    __tablename__ = 'users'
    id = DB.Column(DB.Integer, primary_key=True)
    username = DB.Column(DB.String(64, collation='NOCASE'),
                         nullable=False, unique=True, index=True)
    email = DB.Column(DB.String(255, collation='NOCASE'),
                      nullable=False, unique=True, index=True)
    password = DB.Column(DB.String(128), nullable=False, server_default='')
    active = DB.Column('is_active', DB.Boolean(),
                       nullable=False, server_default='1')
    email_confirmed_at = DB.Column(DB.DateTime())
    first_name = DB.Column(DB.String(100, collation='NOCASE'),
                           nullable=False, server_default='')
    last_name = DB.Column(
        DB.String(100, collation='NOCASE'), nullable=False, server_default='')
    roles = DB.relationship(
        'Role', secondary='user_roles', lazy='dynamic')

    # user_emails = DB.relationship('UserEmail')

    def __str__(self):
        return '<User: {}>'.format(self.username)

    def __repr__(self):
        return '<User: {}>'.format(self.username)

    def has_role(self, role):
        """Check whether a user has a specific role"""
        return role in [r.name for r in self.roles]


class Role(DB.Model):
    """Role model to define Roles"""
    __tablename__ = 'roles'
    id = DB.Column(DB.Integer(), primary_key=True)
    name = DB.Column(DB.String(50), unique=True)

    def __str__(self):
        return '<Role: {}>'.format(self.name)

    def __repr__(self):
        return '<Role: {}>'.format(self.name)


class UserRole(DB.Model):
    """Helper table for User.roles"""
    __tablename__ = 'user_roles'
    id = DB.Column(DB.Integer(), primary_key=True)
    user_id = DB.Column(DB.Integer(), DB.ForeignKey(
        'users.id', ondelete='CASCADE'))
    role_id = DB.Column(DB.Integer(), DB.ForeignKey(
        'roles.id', ondelete='CASCADE'))

    def __str__(self):
        return '<UserRole: {}>'.format(self.id)

    def __repr__(self):
        return '<UserRole: {}>'.format(self.id)


# class UserEmail(DB.Model):
#     """Email for if User can have multiple Emails"""
#     __tablename__ = 'user_emails'
#     id = DB.Column(DB.Integer, primary_key=True)

#     user_id = DB.Column(DB.Integer, DB.ForeignKey('user.id'))
#     user = DB.relationship('User', uselist=False)

#     # User email information
#     email = DB.Column(DB.String(255), nullable=False, unique=True)
#     email_confirmed_at = DB.Column(DB.DateTime())
#     is_primary = DB.Column(DB.Boolean(), nullable=False, server_default='0')
