"""User module to handle user specific"""
from flask import Blueprint


blueprint = Blueprint('user', __name__, template_folder='templates')

# pylint: disable=wrong-import-position
from app.user import routes, models
