"""
App Module
"""
# pylint: disable=wrong-import-position
import os
from functools import wraps
from flask import Flask, flash, session, wrappers, url_for, redirect, request
from flask_babelex import Babel
from flask_mail import Mail
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_user import UserManager
from flask_wtf.csrf import CSRFProtect

from app.utils import log

# ===== Config ===== #
APP = Flask(__name__, static_folder='static/dist', static_url_path='/static')

CONFIG_CACHE = {}
APP.config.from_pyfile('config.py')
CONFIG_CACHE = APP.config.copy()

# ===== Babel ===== #
BABEL = Babel(APP)


@BABEL.localeselector
def get_locale():
    """Babels locale selector"""
    return request.accept_languages.best_match(APP.config.get('LANGUAGES'))


# ===== DB ===== #
DB = SQLAlchemy(APP)
MIGRATE = Migrate(APP, DB)


# ===== CSRF ===== #
CSRF = CSRFProtect(APP)


# ===== Flask-User ===== #
from app.user.models import User
USER_MANAGER = UserManager(APP, DB, User)


# ===== Blueprint Section ===== #
from app.main import blueprint as main_bp
APP.register_blueprint(main_bp)


from app.user import blueprint as user_bp
APP.register_blueprint(user_bp)

# ===== App init ===== #
from app import cli

log(__name__, '{} successfully initialized!'.format(APP.config.get('APP_NAME')))
