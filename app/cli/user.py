"""
User cli module
"""
# pylint: disable=W0622
from datetime import datetime
from sqlalchemy.exc import IntegrityError

# import click

from app import APP, DB, USER_MANAGER
from app.user.models import User, Role


@APP.cli.group()
def user():
    """User commands."""
    pass


@user.command()
def create():
    """
    Create a new User.

    ```bash
    (.venv)$ flask user create
    Username: Admin
    Email:  admin@example.com
    Roles:  admin, agent
    Password:
    Confirm password:
    Success, new user <Admin> created!
    ```
    """
    def pwd():
        """Password with confirmation"""
        from getpass import getpass
        password = getpass('Password: ')
        password2 = getpass('Confirm password: ')
        if not password2 == password:
            print('Passwors weren\'t identical!')
            return pwd()
        return str(password)

    username = input('Username: ')
    email = input('Email: ')
    roles = [r.strip() for r in input('Roles: ').split(',')]
    #password = pwd()
    new_user = User(
        username=username,
        email=email,
        email_confirmed_at=datetime.utcnow(),
        password=USER_MANAGER.hash_password(pwd())
    )

    for role in roles:
        role_exist = Role.query.filter(Role.name == str(role)).first()
        if role_exist is not None:
            new_user.roles.append(role_exist)
        else:
            new_user.roles.append(Role(name=role))
    try:
        DB.session.add(new_user)
        DB.session.commit()
        print('Success, new user <{}> created!'.format(new_user.username))
    except IntegrityError as error:
        print('Soemthing went wrong!')
        print('Due to: {}'.format(error.orig))
