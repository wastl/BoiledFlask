.. BoiledFlask documentation master file, created by
   sphinx-quickstart on Sat Jun 30 13:17:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================================
Welcome to BoiledFlask's documentation!
=======================================

.. toctree::
  :glob:
  :maxdepth: 4
  :caption: Contents:

  Getting Started <usage/getstarted>
  API Documentation <modules>

=======
Indices
=======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
