===============
Getting Started
===============

------------
Dependencies
------------

* Python3 (with pip & virtualenv)
* NodeJS (with npm & **yarn**)

------------
Installation
------------

Activate your `.venv` before proceeding

.. code-block:: bash

  (.venv)$ pip install -r requirements.txt
  (.venv)$ yarn install

-----
Start
-----

.. code-block:: bash

  $ yarn start
