app.cli package
===============

Submodules
----------

app.cli.translate module
------------------------

.. automodule:: app.cli.translate
    :members:
    :undoc-members:
    :show-inheritance:

app.cli.user module
-------------------

.. automodule:: app.cli.user
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: app.cli
    :members:
    :undoc-members:
    :show-inheritance:
