const STATIC_PATH = './app/static';
/**
 * Require gulp and sass
 */
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
/**
 * Require minify helpers
 */
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const rename = require('gulp-rename');

gulp.task('vendorjs', () =>
  gulp
    .src([
      './node_modules/bootstrap/dist/js/bootstrap.js',
      // './node_modules/bootstrap/dist/js/bootstrap.min.js',
      // './node_modules/bootstrap/dist/js/bootstrap.min.js.map',
      './node_modules/jquery/dist/jquery.js',
      // './node_modules/jquery/dist/jquery.min.js',
      // './node_modules/jquery/dist/jquery.min.map',
      './node_modules/popper.js/dist/umd/popper.js',
      // './node_modules/popper.js/dist/umd/popper.min.js',
      // './node_modules/popper.js/dist/umd/popper.min.js.map',
    ])
    .pipe(gulp.dest(`${STATIC_PATH}/dist/js/vendor`)),
);

gulp.task('vendorcss', () =>
  gulp.src([]).pipe(gulp.dest(`${STATIC_PATH}/dist/css/vendor`)),
);

gulp.task('refreshBootstrapVars', () =>
  gulp
    .src(['./node_modules/bootstrap/scss/_variables.scss'])
    .pipe(gulp.dest(`${STATIC_PATH}/src/scss/bootstrap`)),
);

gulp.task('minify', () =>
  gulp
    .src(`${STATIC_PATH}/dist/css/*.css`)
    .pipe(
      rename({
        suffix: '.min',
      }),
    )
    .pipe(sourcemaps.init())
    .pipe(postcss([autoprefixer({ browsers: ['last 2 version'] }), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(`${STATIC_PATH}/dist/css`)),
);

gulp.task('sass', () =>
  gulp
    .src(`${STATIC_PATH}/src/scss/bootstrap/bootstrap-custom.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(`${STATIC_PATH}/dist/css`)),
);

gulp.task('watch', ['sass'], () =>
  gulp.watch([`${STATIC_PATH}/src/scss/**/*.scss`], ['sass']),
);

gulp.task('default', ['watch']);
