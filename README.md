[master]: https://gitlab.com/wastl/BoiledFlask/tree/master
[simple-flask]: https://gitlab.com/wastl/BoiledFlask/tree/simple-flask
[simple-flask-bootstrap]: https://gitlab.com/wastl/BoiledFlask/tree/simple-flask-bootstrap
[pure-flask]: https://gitlab.com/wastl/BoiledFlask/tree/pure-flask
[pure-flask-bootstrap]: https://gitlab.com/wastl/BoiledFlask/tree/pure-flask-bootstrap
[reacted-flask]: https://gitlab.com/wastl/BoiledFlask/tree/REACTed-flask
[flask-user]: http://flask-user.readthedocs.io/en/latest/
[flask-sqlalchemy]: http://flask-sqlalchemy.pocoo.org/2.3/
[flask-wtf]: https://flask-wtf.readthedocs.io/en/stable/
[flask-babelex]: https://pythonhosted.org/Flask-BabelEx/
[flask-mail]: https://pythonhosted.org/flask-mail/
[bootstrap]: http://getbootstrap.com/docs/4.1/getting-started/introduction/
[react]: https://reactjs.org/

# BoiledFlask

A Flask quickstart boilerplate with some helpful features integrated, like Bootstrap 4, Flask-User.

**BoiledFlask** comes in these different flavours (branches):

### [Master] includes:

- [Flask-User] for User authentication with Roles, Invitesystem, Mailconfirmation with [Flask-Mail] and more
- [Flask-SQLAlchemy] as ORM
- [Flask-WTF] for Forms and `CRSF` protection
- [Flask-BabelEx] for Translation
- [Bootstrap] as Frontend Framework, with `SCSS`-build-system

### [REACTed-Flask]:

- [Master] with [React] integrated

### [Simple-Flask] includes:

- [Flask-SQLAlchemy] as ORM
- [Flask-WTF] for Forms and `CRSF` protection
- [Flask-BabelEx] for Translation

### [Pure-Flask] is a pure Flask boilerplate!

### [Simple-Flask] & [Pure-Flask] are also available with [Bootstrap]:

- [Simple-Flask-Bootstrap]
- [Pure-Flask-Bootstrap]

---

## Dependencies

- Python3 (incl. pip & virtualenv)
- NodeJS (incl. **yarn**)

---

## Installation

- Setup the virtual environment folder `.venv` and activate it

- Install the dependencies

  ```bash
  (.venv)$ pip install -r requirements.txt
  # also the node dependencies
  (.venv)$ yarn install
  ```

- Create and Migrate th DB

  ```bash
  (.venv)$ export FLASK_APP=app.py
  (.venv)$ flask db init
  (.venv)$ flask db migrate
  (.venv)$ flask db upgrade
  ```

- Create a User with Roles (comma seperated)

  ```bash
  (.venv)$ flask user create
  # follow instructions
  ```

- Handle translations

  ```bash
  # add a new translation language
  (.venv)$ flask translate init <lang>
  # update all available translations
  (.venv)$ flask translate update
  # compile all translations
  (.venv)$ flask translate compile
  ```

---

## Usage

- Start the development environment

  ```bash
  $ yarn start
  ```

  edit `dev.sh` if you named your env different than `.venv`:

  ```bash
  ./.venv/bin/flask run -h "0.0.0.0" -p 3000
  ```
